/*******************************************************************************
 * Copyright (c) 2012 Evelina Vrabie
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *******************************************************************************/
package org.codeandmagic.android.gauge;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.BlurMaskFilter;
import android.graphics.BlurMaskFilter.Blur;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;

public class TachoView extends GaugeView{

	public static final String DEFAULT_SPEED_UNITS = "mph";
	public static final int DEFAULT_GEAR_COLOUR_FOREGROUND = Color.RED;
	public static final int DEFAULT_GEAR_COLOUR_BACKGROUND = Color.parseColor("#666E6E6E"); 
	public static final String DEFAULT_GEAR_FONT_FOREGROUND = "fonts/trana.TTF";
	public static final String DEFAULT_GEAR_FONT_BACKGROUND = "fonts/trana1.TTF";
	public static final float DEFAULT_WARN_RPM = 0.9f;
	public static final int DEFAULT_SCALE_DIGET_COLOUR = Color.WHITE;
	public static final int USE_SCALE_PAINT_FOR_DIGITS = 1;
	public static final boolean DEFAULT_ROTATE_DIVISION_NUMBERS_WITH_FACE = true;
	public static final boolean DEFAULT_ITALICS_SCALE_DIVISIONS = false;
	public static final float[] DEFAULT_SHIFT_LIGHT_UNLIT_POSITIONSIZE = new float[]{0.8f,0.65f,0.03f};
	public static final float[] DEFAULT_SHIFT_LIGHT_LIT_POSITIONSIZE = new float[]{0.8f,0.65f,0.032f}; 
	private static final float DEFAULT_SUBDIVISION_TICK_LENGTH = 0.015f;
	private static final float DEFAULT_DIVISION_TICK_LENGTH = 0.045f;
	private AttributeSet attributes;

	/**Paint to use when the shift light is not lit*/
	private Paint mShiftLightUnlitPaint;
	/**Paint to use when the shift light should be lit*/
	private Paint mShiftLightLitPaint;
	/**Paint to use on border of shift light*/
	private Paint mShiftLightUnlitPaintBorder;
	/**Paint to use on shadown of unlit shift light*/
	private Paint mShiftLightUnlitShadowPaint;
	/**Paint to use for the foreground of the gear indicator*/
	private Paint mGearNumberPaint;
	/**Paint to use for the background of the gear indicator*/
	private Paint mGearNumberGridPaint;
	/**Paint to use to draw the speed*/
	private Paint mSpeedPaint;
	/**Paint to use to draw the speed uni*/
	private Paint mSpeedUnitPaint;
	/**Paint to use if the user does NOT want the scale digits painted the same as the division lines*/
	private Paint mScaleDigetPaint;

	/**Colour of the foreground gear indicator*/
	private int gearColourForeground;
	/**Colour of the background gear indicator*/
	private int gearColourBackground;
	/**String to path to the font used for the foreground of gear indicator*/
	private String gearFontForeground;
	/**String to path to the font used for the background of gear indicator*/
	private String gearFontBackground;
	/**Colour used for the digits on the scale if NOT using scale paint*/
	private int scaleDigitColor;
	/**String to path of font used for scale digits*/
	private String scaleDigitFontPath;
	/**Length used to draw subdivision ticks*/
	private float subdivisionTickLength;
	/**Length used to draw numbered division tick length*/
	private float divisionTickLength;

	private BlurMaskFilter shiftLightMaskFilter;

	private int useScalePaintForDigit;
	private boolean rotateDivisionNumbersWithFace;
	private boolean italicsScaleDivisionNumbers;

	private String gear = "0";
	private String gearBackground = "0";
	private int speed = 0;
	private String speedString;
	private int maxRpm;
	private String speedUnits;
	private float warnRpm;
	private float warnRpmLimit = 0;

	private float[] shiftLightLitPositionSize = DEFAULT_SHIFT_LIGHT_LIT_POSITIONSIZE;
	private float[] shiftLightUnlitPositionSize = DEFAULT_SHIFT_LIGHT_UNLIT_POSITIONSIZE;

	public TachoView(final Context context, final AttributeSet attrs, final int defStyle) {
		super(context, attrs, defStyle);
	}

	public TachoView(final Context context, final AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public TachoView(final Context context) {
		this(context, null, 0);
	}

	@Override
	protected void readAttrs(final Context context, final AttributeSet attrs, final int defStyle) {
		super.readAttrs(context, attrs, defStyle);
		final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TachoView, defStyle, 0);
		if(a.hasValue(R.styleable.TachoView_speedUnitsString)){
			speedUnits = a.getString(R.styleable.TachoView_speedUnitsString);
		}else{speedUnits = DEFAULT_SPEED_UNITS;}

		if(a.hasValue(R.styleable.TachoView_gearColourForeground)){
			gearColourForeground = Color.parseColor(a.getString(R.styleable.TachoView_gearColourForeground));
		}else{gearColourForeground = DEFAULT_GEAR_COLOUR_FOREGROUND;}
		if(a.hasValue(R.styleable.TachoView_gearColourBackground)){
			gearColourBackground = Color.parseColor(a.getString(R.styleable.TachoView_gearColourBackground));
		}else{gearColourBackground = DEFAULT_GEAR_COLOUR_BACKGROUND;}

		if(a.hasValue(R.styleable.TachoView_gearFontForeground)){
			gearFontForeground = a.getString(R.styleable.TachoView_gearFontForeground);
		}else{gearFontForeground = DEFAULT_GEAR_FONT_FOREGROUND;}
		if(a.hasValue(R.styleable.TachoView_gearFontBackground)){
			gearFontBackground = a.getString(R.styleable.TachoView_gearFontBackground);
		}else{gearFontBackground = DEFAULT_GEAR_FONT_BACKGROUND;}

		rotateDivisionNumbersWithFace = a.getBoolean(R.styleable.TachoView_rotateDivisionNumbersWithFace, DEFAULT_ROTATE_DIVISION_NUMBERS_WITH_FACE);

		warnRpm = a.getFloat(R.styleable.TachoView_warnRpmPercent, DEFAULT_WARN_RPM);

		if(a.hasValue(R.styleable.TachoView_scaleDigitColour)){
			useScalePaintForDigit = 0;
			scaleDigitColor = Color.parseColor(a.getString(R.styleable.TachoView_scaleDigitColour));
		}else{useScalePaintForDigit = USE_SCALE_PAINT_FOR_DIGITS;}

		italicsScaleDivisionNumbers = a.getBoolean(R.styleable.TachoView_italicScaleDigits, DEFAULT_ITALICS_SCALE_DIVISIONS);

		if(a.hasValue(R.styleable.TachoView_scaleDigitsFont)){
			scaleDigitFontPath = a.getString(R.styleable.TachoView_scaleDigitsFont);
		}else{scaleDigitFontPath = null;}

		divisionTickLength = a.getFloat(R.styleable.TachoView_divisionTickLength, DEFAULT_DIVISION_TICK_LENGTH);
		subdivisionTickLength = a.getFloat(R.styleable.TachoView_subdivisionTickLength,DEFAULT_SUBDIVISION_TICK_LENGTH);

		a.recycle();
		this.attributes = attrs;
	}

	protected void initDrawingTools() {
		super.initDrawingTools();

		mShiftLightUnlitPaint = getDefaultShiftLightUnlitPaint(); 
		mShiftLightLitPaint = getDefaultShiftLightLitPaint();
		shiftLightMaskFilter = new BlurMaskFilter(0.025f, Blur.SOLID);
		mShiftLightLitPaint.setMaskFilter(shiftLightMaskFilter);
		mShiftLightUnlitPaintBorder = getDefaultShiftLightUnlitBorderPaint();

		mGearNumberPaint = getDefaultGearPaint();
		mGearNumberGridPaint = getDefaultGearGridPaint();

		mSpeedPaint = getDefaultSpeedPaint();
		mSpeedUnitPaint = getDefaultSpeedUnitPaint();
		mShiftLightUnlitShadowPaint = getDefaultShiftLightUnlitShadow();

		mScaleDigetPaint = getDefaultScaleDigetPaint();
	}
	private Paint getDefaultSpeedPaint(){
		Typeface fontit1 = Typeface.create(Typeface.SANS_SERIF, Typeface.ITALIC);
		final Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
		paint.setTypeface(fontit1);
		paint.setStyle(Paint.Style.FILL);
		paint.setTextSize(0.1f);
		paint.setColor(Color.WHITE);
		paint.setTextAlign(Align.CENTER);
		return paint;
	}
	private Paint getDefaultSpeedUnitPaint(){
		Typeface fontit1 = Typeface.create(Typeface.SANS_SERIF, Typeface.ITALIC);
		final Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
		paint.setTypeface(fontit1);
		paint.setStyle(Paint.Style.FILL);
		paint.setTextSize(0.06f);
		paint.setColor(Color.WHITE);
		paint.setTextAlign(Align.CENTER);

		return paint;
	}
	private Paint getDefaultShiftLightLitPaint(){
		final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(Color.RED);
		return paint;
	}
	private Paint getDefaultShiftLightUnlitPaint(){
		final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(Color.parseColor("#1C1C1C"));
		return paint;
	}
	public Paint getDefaultShiftLightUnlitShadow(){
		final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setShader(new RadialGradient(0.8f, 0.65f, 0.05f, new int[] { Color.argb(60, 40, 96, 170),
				Color.argb(80, 15, 34, 98), Color.argb(120, 0, 0, 0), Color.argb(140, 0, 0, 0) },null, TileMode.MIRROR));
		return paint;
	}
	private Paint getDefaultShiftLightUnlitBorderPaint(){
		final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setStyle(Paint.Style.STROKE);
		paint.setColor(Color.BLACK);
		paint.setStrokeWidth(0.003f);
		return paint;
	}
	private Paint getDefaultGearPaint(){
		Typeface fontFace = Typeface.createFromAsset(getResources().getAssets(),gearFontForeground);
		Typeface fontFaceBold = Typeface.create(fontFace, Typeface.BOLD);
		Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
		paint.setColor(gearColourForeground);
		paint.setStyle(Paint.Style.FILL);
		paint.setStrokeWidth(0.0045f);
		paint.setTextSize(0.27f);
		paint.setTypeface(fontFaceBold);
		paint.setTextAlign(Align.CENTER);
		paint.setShadowLayer(0.005f, 0.002f, 0.002f, mTextShadowColor);
		return paint;
	}
	private Paint getDefaultGearGridPaint(){
		Typeface fontGrid = Typeface.createFromAsset(getResources().getAssets(),gearFontBackground);
		Typeface fontGridBold = Typeface.create(fontGrid, Typeface.BOLD);
		final Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
		paint.setColor(gearColourBackground);
		paint.setStyle(Paint.Style.FILL);
		paint.setStrokeWidth(0.0045f);
		paint.setTextSize(0.27f);
		paint.setTypeface(fontGridBold);
		paint.setTextAlign(Align.CENTER);
		return paint;
	}
	private Paint getDefaultScaleDigetPaint(){
		Typeface font;
		if(scaleDigitFontPath == null){
			font = Typeface.SANS_SERIF;
		}else{
			font = Typeface.createFromAsset(getResources().getAssets(), scaleDigitFontPath);
		}

		if(italicsScaleDivisionNumbers){
			font = Typeface.create(font, Typeface.ITALIC);
		}

		final Paint paint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
		paint.setColor(scaleDigitColor);
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		paint.setStrokeWidth(0.005f);
		paint.setTextSize(mScaleRangeTextSize);
		paint.setTypeface(font);
		paint.setTextAlign(Align.CENTER);
		paint.setShadowLayer(0.005f, 0.002f, 0.002f, mTextShadowColor);
		return paint;
	}

	@Override
	protected void initScale() {
		mScaleRotation = (360-mScaleStartAngle)+mScaleEndAngle;
		mSubdivisions = (int)(mScaleEndValue / mSubdivisionValue);
		mSubdivisionAngle = mScaleRotation / mSubdivisions;
		//maxRpm = (int) mScaleEndValue;
//		log("ScaleRotation: "+mScaleRotation);
//		log("ScaleEndValue: "+mScaleEndValue);
//		log("ScaleDivisionValue: "+mDivisionValue);
//		log("SubDivisionValue: "+mSubdivisionValue);
//		log("SubDivisions: "+mSubdivisions);
//		log("SubDivisionAngle: "+mSubdivisionAngle);
	}

	@Override
	protected void onDraw(final Canvas canvas) {
		mCurrentValue = mTargetValue;
		super.doDraw(canvas);

		if(mTargetValue < warnRpmLimit){
			canvas.drawCircle(shiftLightUnlitPositionSize[0],shiftLightUnlitPositionSize[1],shiftLightUnlitPositionSize[2],mShiftLightUnlitPaint);
			canvas.drawCircle(shiftLightUnlitPositionSize[0],shiftLightUnlitPositionSize[1],shiftLightUnlitPositionSize[2], mShiftLightUnlitPaintBorder);
			canvas.drawCircle(shiftLightUnlitPositionSize[0],shiftLightUnlitPositionSize[1],shiftLightUnlitPositionSize[2], mShiftLightUnlitShadowPaint);
		}else{
			canvas.drawCircle(shiftLightLitPositionSize[0],shiftLightLitPositionSize[1],shiftLightLitPositionSize[2],mShiftLightLitPaint);
		}

		canvas.drawText(gearBackground, 0.7f, 0.67f, mGearNumberGridPaint);
		canvas.drawText(gear, 0.7f, 0.67f, mGearNumberPaint);
		canvas.drawText(speedUnits, 0.7f, 0.72f, mSpeedUnitPaint);

		canvas.drawText(speedString, 0.55f, 0.75f, mSpeedPaint);
	}

	@Override
	protected float getAngleForValue(final float value) {
		float usedPercent = (value / mScaleEndValue);
		float degsToAdd = mScaleRotation*usedPercent;
		if((mScaleStartAngle + degsToAdd) > 360){
			return (mScaleStartAngle+degsToAdd)-360;
		}else{
			return mScaleStartAngle+degsToAdd;
		}
	}

	@Override
	protected void drawScale(final Canvas canvas) {
		canvas.save(Canvas.MATRIX_SAVE_FLAG);
		// On canvas, North is 0 degrees, East is 90 degrees, South is 180 etc.
		// We start the scale somewhere South-West so we need to first rotate the canvas.
		canvas.rotate(mScaleStartAngle, 0.5f, 0.5f);
		int subDivisionCounter = 0;
		float back2zero;
		Paint tempPaint;
		for (int i = 0; i <= mScaleEndValue; i+=mSubdivisionValue) {
			final float y1 = mScaleRect.top;
			final float y2 = y1 + subdivisionTickLength; // height of division
			final float y3 = y1 + divisionTickLength; // height of subdivision

			if(useScalePaintForDigit == USE_SCALE_PAINT_FOR_DIGITS){
				tempPaint = getRangePaint(i);
			}else{
				tempPaint = mScaleDigetPaint;
			}
			if (0 == i % mDivisionValue) {
				// Draw a division tick
				canvas.drawLine(0.5f, y1, 0.5f, y3, tempPaint);
				// Draw the text 0.15 away from the division tick

				//Get the text bounds to know how much to shimmy it by
				//Scale the paints text size to get around our canvas scaling
				Rect paintBounds = new Rect();
				float textSize = tempPaint.getTextSize();
				tempPaint.setTextSize(textSize * mScale);
				//Get the bounds, and restore the text size
				tempPaint.getTextBounds(valueString(i/1000), 0, valueString(i/1000).length(), paintBounds);
				tempPaint.setTextSize(textSize);

				if(rotateDivisionNumbersWithFace){
					if(useScalePaintForDigit == USE_SCALE_PAINT_FOR_DIGITS){
						canvas.drawText(valueString(i/1000), 0.5f, y3 + divisionTickLength +((paintBounds.height()/mScale)/2), tempPaint);
					}else{
						canvas.drawText(valueString(i/1000), 0.5f, y3 + divisionTickLength +((paintBounds.height()/mScale)/2), mScaleDigetPaint);
					}
				}else{
					canvas.save();
					//Calculate how much to rotate the canvas by to straighten the number again after rotating for subdivisions
					back2zero = (-mScaleStartAngle)-(subDivisionCounter*mSubdivisionAngle);
					canvas.rotate(back2zero, 0.5f, y3 + divisionTickLength);
					if(back2zero < (-360)){
						//Past the vertical, shimmy the text left to keep it inline
						if(y3 + divisionTickLength < 0.5){
							//past the horizontal, shimmy the text south to keep it inline
							canvas.drawText(valueString(i/1000), 0.5f-((paintBounds.width()/mScale)/4), y3 + divisionTickLength +((paintBounds.height()/mScale)/2), tempPaint);
						}else{
							canvas.drawText(valueString(i/1000), 0.5f-((paintBounds.width()/mScale)/2), y3 + divisionTickLength, tempPaint);
						}
					}else{
						if(y3 + divisionTickLength < 0.5){
							//also shimmy the left side down if its above the horizontal
							canvas.drawText(valueString(i/1000), 0.5f, y3 + divisionTickLength +((paintBounds.height()/mScale)/2), tempPaint);
						}else{
							canvas.drawText(valueString(i/1000), 0.5f, y3 + divisionTickLength -((paintBounds.height()/mScale)/2), tempPaint);
						}
					}
					canvas.restore();
				}
			}
			else {
				// Draw a subdivision tick
				canvas.drawLine(0.5f, y1, 0.5f, y2, getRangePaint(i));
			}
			canvas.rotate(mSubdivisionAngle, 0.5f, 0.5f);
			subDivisionCounter++;
		}
		canvas.restore();
		canvas.drawText("x1000", 0.5f, 0.45f, mSpeedUnitPaint);
	}

	public void setRpm(float rpm){
		super.setTargetValue(rpm);
	}
	public void setMaxRpm(int maxRpm){
		if(this.maxRpm != maxRpm){
			Log.e("TACH","Max not ==! "+this.maxRpm+" vs "+maxRpm);
			this.maxRpm = maxRpm;
			this.warnRpmLimit  = warnRpm * maxRpm;
			mScaleEndValue = (maxRpm + 600) / 1000 * 1000;
			initScale();
			drawGauge();
		}
	}
	public void setGear(int g){
		if(g == -1){gear = "R";}
		if(g == 0){gear = "N";}
		if(g > 0){
			gear = String.valueOf(g);
		}
	}
	public void setSpeed(int s){
		this.speed = s;
		this.speedString = String.valueOf(speed);
	}
	public void setSpeedUnits(String speedUnits){
		this.speedUnits = speedUnits;
	}
	public AttributeSet getAttributes(){
		return this.attributes;
	}
	public void redrawGauge(){
		super.drawGauge();
	}

	private void log(String msg){
		Log.e("Tacho",msg);
	}

	public void readPreferences(SharedPreferences prefs){
		this.speedUnits = prefs.getString(PropertyKeys.SpeedUnitString, DEFAULT_SPEED_UNITS);
		this.gearColourForeground = prefs.getInt(PropertyKeys.GearColourForeground, DEFAULT_GEAR_COLOUR_FOREGROUND);
		this.gearColourBackground = prefs.getInt(PropertyKeys.GearColourBackgound, DEFAULT_GEAR_COLOUR_BACKGROUND);
		this.gearFontForeground = prefs.getString(PropertyKeys.GearFontForeground, DEFAULT_GEAR_FONT_FOREGROUND);
		this.gearFontBackground = prefs.getString(PropertyKeys.GearFontBackground, DEFAULT_GEAR_FONT_BACKGROUND);
		this.rotateDivisionNumbersWithFace = prefs.getBoolean(PropertyKeys.RotateDivisionNumbersWithFace, DEFAULT_ROTATE_DIVISION_NUMBERS_WITH_FACE);
		this.warnRpm = prefs.getFloat(PropertyKeys.WarnRPMPercent, DEFAULT_WARN_RPM);
		this.scaleDigitColor = prefs.getInt(PropertyKeys.ScaleDigitColour, DEFAULT_SCALE_DIGET_COLOUR);
		this.italicsScaleDivisionNumbers = prefs.getBoolean(PropertyKeys.ItalicsScaleDigits, DEFAULT_ITALICS_SCALE_DIVISIONS);
		this.scaleDigitFontPath = prefs.getString(PropertyKeys.ScaleDigitsFont, null);
		initDrawingTools();
		super.drawGauge();
		invalidate();
	}

	public SharedPreferences getPreferences(Context context){
		SharedPreferences prefs = context.getSharedPreferences("tacho_dial", Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = prefs.edit();

		edit.putString(PropertyKeys.SpeedUnitString, speedUnits);
		edit.putInt(PropertyKeys.GearColourForeground, gearColourForeground);
		edit.putInt(PropertyKeys.GearColourBackgound, gearColourBackground);
		edit.putString(PropertyKeys.GearFontForeground, gearFontForeground);
		edit.putString(PropertyKeys.GearFontBackground, gearFontBackground);
		edit.putFloat(PropertyKeys.WarnRPMPercent, warnRpm);
		edit.putInt(PropertyKeys.ScaleDigitColour, scaleDigitColor);
		edit.putBoolean(PropertyKeys.RotateDivisionNumbersWithFace, rotateDivisionNumbersWithFace);
		edit.putBoolean(PropertyKeys.ItalicsScaleDigits, italicsScaleDivisionNumbers);
		edit.putString(PropertyKeys.ScaleDigitsFont, scaleDigitFontPath);
		edit.commit();

		return prefs;
	}
	public static class PropertyKeys{
		public final static String SpeedUnitString = 		"SpeedUnitString";
		public final static String GearColourForeground = 	"GearColourForeground";
		public final static String GearColourBackgound = 	"GearColourBackround";
		public final static String GearFontForeground =		"GearFontForeground";
		public final static String GearFontBackground = 	"GearFontBackground";
		public final static String WarnRPMPercent = 		"WarnRPMPercent";
		public final static String ScaleDigitColour = 		"ScaleDigitColour";
		public final static String RotateDivisionNumbersWithFace = "RotateDivisionNumbersWithFace";
		public final static String ItalicsScaleDigits = "ItalicScaleDigits";
		public final static String ScaleDigitsFont = "ScaleDigitsFont";
	}
}

